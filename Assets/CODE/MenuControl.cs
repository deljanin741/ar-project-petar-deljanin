using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuControl : MonoBehaviour
{
    public GameObject GameCanvasUI;

    public GameObject MenuCanvasUI;

    public bool menuCanvas = false;

    public void SwitchUI()
    {
        if (menuCanvas == true)
        {
            MenuCanvasUI.SetActive(true);
            GameCanvasUI.SetActive(false);
            menuCanvas = false;
            Time.timeScale = 0F;
        }
        else
        {
            MenuCanvasUI.SetActive(false);
            GameCanvasUI.SetActive(true);
            menuCanvas = true;
            Time.timeScale = 1F;
        }
    }
}
