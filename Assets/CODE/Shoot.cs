﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shoot : MonoBehaviour
{
    public GameObject arCamera;

    private RaycastHit hit;

    MyPokemon MP;

    public Text articunoText;

    public Text bulbasaurText;

    public Text charizardText;

    public Text pikachuText;

    public Text scytherText;

    public Text mewtwoText;

    public Text gyradosText;

    public void shoot()
    {
        MP = GameObject.Find("MyPokemonScript").GetComponent<MyPokemon>();
        if (
            Physics
                .Raycast(arCamera.transform.position,
                arCamera.transform.forward,
                out hit)
        )
        {
            Debug.Log("FIRST if");
            if (
                hit.transform.name == "articuno(Clone)" ||
                hit.transform.name == "bulbasaur(Clone)" ||
                hit.transform.name == "charizard(Clone)" ||
                hit.transform.name == "pikachu(Clone)" ||
                hit.transform.name == "scyther(Clone)" ||
                hit.transform.name == "mewtwo(Clone)" ||
                hit.transform.name == "gyrados(Clone)"
            )
            {
                Debug.Log("SECOND if");
                checkPokemon(hit.transform.name);

                Destroy(hit.transform.gameObject);
            }
        }
    }

    public void checkPokemon(string pokemonName)
    {
        MP = GameObject.Find("MyPokemonScript").GetComponent<MyPokemon>();

        if (pokemonName == "articuno(Clone)")
        {
            MP.myPokemon[0][MP.a] = Random.Range(1, 101);
            MP.a++;
            articunoText.text += print(MP.myPokemon[0]);
        }
        else if (pokemonName == "bulbasaur(Clone)")
        {
            MP.myPokemon[1][MP.b] = Random.Range(1, 101);
            MP.b++;
            bulbasaurText.text += print(MP.myPokemon[1]);
        }
        else if (pokemonName == "charizard(Clone)")
        {
            MP.myPokemon[2][MP.c] = Random.Range(1, 101);
            MP.c++;
            charizardText.text += print(MP.myPokemon[2]);
        }
        else if (pokemonName == "pikachu(Clone)")
        {
            MP.myPokemon[3][MP.p] = Random.Range(1, 101);
            MP.p++;
            pikachuText.text += print(MP.myPokemon[3]);
        }
        else if (pokemonName == "scyther(Clone)")
        {
            MP.myPokemon[4][MP.p] = Random.Range(1, 101);
            MP.s++;
            scytherText.text += print(MP.myPokemon[4]);
        }
        else if (pokemonName == "mewtwo(Clone)")
        {
            MP.myPokemon[5][MP.p] = Random.Range(1, 101);
            MP.m++;
            mewtwoText.text += print(MP.myPokemon[5]);
        }
        else if (pokemonName == "gyrados(Clone)")
        {
            MP.myPokemon[6][MP.p] = Random.Range(1, 101);
            MP.g++;
            gyradosText.text += print(MP.myPokemon[6]);
        }
    }

    public string print(int[] x)
    {
        string temp = "";
        foreach (var y in x)
        {
            if (y == 0) break;
            temp += " " + y.ToString();
        }
        Debug.Log (temp);
        return temp;
    }
}
