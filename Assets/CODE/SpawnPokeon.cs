using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPokeon : MonoBehaviour
{
    public Transform spawnPoint;

    public GameObject[] Pokemon;

    public GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        Pokemon[0].transform.localScale = new Vector3(0.02F, 0.02F, 0.02F);
        Pokemon[1].transform.localScale = new Vector3(0.1F, 0.1F, 0.1F);
        Pokemon[2].transform.localScale = new Vector3(0.05F, 0.05F, 0.05F);
        Pokemon[3].transform.localScale = new Vector3(0.02F, 0.02F, 0.02F);
        Pokemon[4].transform.localScale = new Vector3(0.1F, 0.1F, 0.1F);
        Pokemon[5].transform.localScale = new Vector3(0.075F, 0.075F, 0.075F);
        Pokemon[6].transform.localScale = new Vector3(0.075F, 0.075F, 0.075F);

        StartCoroutine(StartSpawning());
    }

    // Update is called once per frame
    void Update()
    {
    }

    IEnumerator StartSpawning()
    {
        int x = (int) Mathf.Floor(Random.Range(0, 7));

        yield return new WaitForSeconds(3); // <-------------------------------------------------------

        Instantiate(Pokemon[x], spawnPoint.position, player.transform.rotation)
            .transform
            .LookAt(player.transform);

        changeSpawnPosition(); // <-------------------------------------------------------
        StartCoroutine(StartSpawning());
    }

    void changeSpawnPosition()
    {
        float
            x,
            y,
            z;
        x = Random.Range(-5.0F, 5.0F);
        y = Random.Range(1.0F, 2.0F);
        z = Random.Range(-5.0F, 5.0F);
        this.spawnPoint.position = new Vector3(x, y, z);
    }
}
