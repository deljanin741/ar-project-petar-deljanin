You can download the apk file from the following github link:
https://github.com/deljanin/AR_Project_Petar_Deljanin

To start the .apk file. 
1) Transfer the file to your android device.
2) Open application for managing files on your andorid device.
3) Find the AR_Petar_Deljanin.apk file and tap on it.
4) Tap install on the dialog box.
5) Go back to home screen and run the AR_Project app with unity icon.


YouTube URLs:
https://youtu.be/zXx_HQlg3PQ
https://youtu.be/YKUeQ-koCDw